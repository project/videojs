<?php

namespace Drupal\videojs\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'videojs_player_list' formatter.
 *
 * @FieldFormatter(
 *   id = "videojs_player_list",
 *   label = @Translation("Video.js Player"),
 *   field_types = {
 *     "file",
 *     "video"
 *   }
 * )
 */
class VideoJsPlayerListFormatter extends VideoJsPlayerFormatter implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $files = $this->getEntitiesToView($items, $langcode);

    // Early opt-out if the field is empty.
    if (empty($files)) {
      return $elements;
    }

    // Collect cache tags to be added for each item in the field.
    $video_items = [];
    foreach ($files as $file) {
      $video_uri = $file->getFileUri();
      $video_items[] = Url::fromUri($this->fileUrlGenerator->generateAbsoluteString($video_uri));
    }
    $elements[] = [
      '#theme' => 'videojs',
      '#items' => $video_items,
      '#player_attributes' => $this->getSettings(),
      '#attached' => [
        'library' => ['videojs/videojs'],
      ],
    ];
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return $field_definition->isList();
  }

}
